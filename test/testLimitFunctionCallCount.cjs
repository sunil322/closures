const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function callbackFunction(){
    return "Callback function called";
}

try{
    const callbackInvoke = limitFunctionCallCount(callbackFunction,2);
    console.log(callbackInvoke(10,20,30));
    console.log(callbackInvoke(10,20));
    console.log(callbackInvoke(20));
}
catch(error){
    console.log(error);
}