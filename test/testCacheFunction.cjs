const cacheFunction = require('../cacheFunction.cjs');

function callbackFunction(){
    return "callback function called";
}
try{
    const invokeCallBack = cacheFunction(callbackFunction);
    console.log(invokeCallBack(10));
    console.log(invokeCallBack(10));
}catch(error){
    console.log(error);
}