function limitFunctionCallCount(callbackFunction,invokeTimes){
    if(typeof callbackFunction === 'function' && typeof invokeTimes === 'number' ){
        return function invokeCallBack(...argumentList){
            if(invokeTimes>=1){
                invokeTimes--;
                return callbackFunction(...argumentList);
            }else{
                return null;
            }
        };
    }else{
        throw Error("The passed parameters to the function are not correct");
    }
}

module.exports = limitFunctionCallCount;

