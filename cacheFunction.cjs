function cacheFunction(callbackFunction){
    let cacheObject = {};
    if(typeof callbackFunction === 'function'){
        return function invokeCallBack(...argumentList){
            if(cacheObject.hasOwnProperty(argumentList)){
                return cacheObject[argumentList];
            }else{
                const result = callbackFunction(...argumentList);
                cacheObject[argumentList] = result;
                return result;
            }
        };
    }else{
        throw Error("Paramters are not correct");
    }
}

module.exports = cacheFunction;
