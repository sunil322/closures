function counterFactory(){
    let counterValue = 0;
    return({
        increment(){
            return counterValue+=1;
        },
        decrement(){
            return counterValue-=1;
        }
    })
}

module.exports = counterFactory;